================================================================================
#   Membri Gruppo:              Matricola:
================================================================================

1.  Luca Samuele Vanzo          (735924)
2.  Marco Piovani               (735380)
3.  Matteo D'Aniello Copreni    (735235)

================================================================================
#   Progetto Prolog:            Parser URI
================================================================================

Il progetto si compone di alcuni predicati, tra cui parsed_uri(URIString, URI),
il quale provvede a splittare la stringa data in input in 7 segmenti secondo 
la struttura "uri" cos� definita: 

    - Scheme
    - Userinfo
    - Host
    - Port
    - Path
    - Query
    - Fragment

================================================================================

La scomposizione in segmenti avviene grazie ad alcuni predicati secondari 
i quali si limitano a controllare il proprio segmento e, nel caso, prevedere
l'andamento dello stato del predicato/segmento successivo. In particolare:		
    
    - scheme        (Lista, URI)
        ->  mailto
        ->  news
        ->  tel
        ->  fax
        ->  generic
    
    - authority     (Lista, URI)
        ->  tel
        ->  fax
        ->  generic
        
    - host          (Lista, URI)
        ->  mailto
        ->  news
        ->  generic
        
    - path          (Lista, URI)
    - port          (Lista, URI)
    - query         (Lista, URI)
    - pragma        (Lista, URI)

Ogni predicato riceve in input una lista [H|T], dove H � il carattere da 
esaminare e T � la lista contenente i restanti caratteri, e la struttura uri 
che scorre la ricorsione.

================================================================================

Lo smistamento per tipologia avviene mediante 
predicato "uri_type" seconda la seguente logica:


URI_TYPE :----( is_[tel|fax]_type )--> tf_scheme( ... )
           \
        is_mail_type ----------------> mail_scheme( ... )
             \
          is_news_type --------------> news_scheme( ... )
               \
            generic -----------------> scheme( ... )
              

I singoli controlli sullo schema si limitano a cercare 
ricorsivamente le parole: "tel:", "fax:", "mailto:" e "news:".
In caso di ricerca negativa passa il controllo a Scheme (generico).

================================================================================

Per verificare la presenta dell'authority si utilizza lo stesso procedimento
nel predicato "c_auth( Lista, Backup, URI )", dove: Lista � fisicamente la
lista in cui lavora per controllare l'esistenza del segmento cercato, Backup
� la lista data in input a c_auth che verr� passata ai predicati authority o 
host, mentre URI � la nostra struttura di 7 segmenti.

================================================================================
            
Infine elenchiamo alcuni predicati utili per il controllo del nostro URI:

    - is_pragma     : Controlla che sia un #
    - is_quest      : Controlla che sia un ?
    - is_auth       : Controlla che sia una @
    - is_slash      : Controlla che sia uno /
    - is_dpnt       : Controlla che sia un :
    - is_point      : Controlla che sia un .
    
    - is_dslash     : Controlla che sia una sequenza di /
    - is_seqpnt     : Controlla che sia una sequenza di .
    - is_seqnot     : Controlla che sia una sequenza non valida dopo il .
    - is_segseq     : Controlla che sia una sequenza di segmenti
    
	- head_cmp      : Paragona il 1� carattere della lista con un altro char
	- tail_rtn      : Ritorna la lista priva del primo carattere
	- is_c_digit    : Controlla se il carattere in input � un numero
    
    
