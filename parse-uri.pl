%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                            MEMBRI GRUPPO PROGETTO                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%    NOME/COG:  Luca Samuele Vanzo (735924)                                    %
%    NOME/COG:  Marco Piovani (735380)                                         %
%    NOME/COG:  Matteo D'Aniello Copreni (735235)                              %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           INFORMAZIONI DI VERSIONE                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%    VER.FILE:  33                                                             %
%    BASE.REV:  C8CC7629FFD9                                                   %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       INFORMAZIONI/RICHIESTE PROGETTO                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%    == Introduzione:                                                          %
%    Navigare in Internet, ma non solo, richiede ad un programma ed            %
%    ai suoi programmatori l’abilità di manipolare delle stringhe che          %
%    rappresentano degli “Universal Resource Identifiers” (URI).               %
%    Lo scopo di questo progetto è di realizzare una libreria in Prolog,       %
%    che costruisca dei termini che rappresentino internamente delle           %
%    URI a partire dalla loro rappresentazione come stringhe.                  %
%    Costruire un parser per le URI semplificate che abbiamo descritto,        %
%    richiede la costruzione di un automa a stati finiti.                      %
%                                                                              %
%                                                                              %
%    URI            ::= scheme ':' authorithy ['/'[path][query][fragment]]     %
%                   |   scheme ':' ['/'][path][query][fragment]                %
%                   |   scheme ':' scheme-syntax                               %
%                                                                              %
%    scheme         ::= ID                                                     %
%                                                                              %
%    authority      ::= '//' [userinfo '@'] host [':' port]                    %
%    userinfo       ::= ID                                                     %
%                                                                              %
%    host           ::= host-id ('.' host-id)* | indirizzo-IP                  %
%    host-id        ::= (carattere diverso '.', '/', '?', '#', '@', e ':')+    %
%    port           ::= digit+                                                 %
%    path           ::= ID ('/' ID)*                                           %
%    query          ::= '?' (carattere diverso da '#')+                        %
%    fragment       ::= '#' carattere+                                         %
%                                                                              %
%    indirizzo-IP   ::= X X X '.' X X X '.' X X X '.' X X X                    %
%    X              ::= digit                                                  %
%                                                                              %
%    ID             ::= (carattere diverso '/', '?', '#', '@', e ':')+         %
%    digit          ::= 0|1|2|3|4|5|6|7|8|9                                    %
%                                                                              %
%    scheme-syntax  ::= sintassi speciale                                      %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                             PIATTAFORME USATE                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%    => MAC OSX Lion                                                           %
%    => Xbuntu 12.10                                                           %
%    => Windows 7                                                              %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                               STRUTTURA URI                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   ----------------------------------------------------------------------------
%   Predicato costante contenente la struttura a segmenti del nostro URI.
%   ----------------------------------------------------------------------------

uri(_, _, _, _, _, _, _).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                              CONTROLLI ATOMI                                 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   ----------------------------------------------------------------------------
%   Caratteri di controllo per ricorsioni.
%   ----------------------------------------------------------------------------

is_pragma(C)    :- string_to_atom([C], D), D = '#'.
is_quest(C)     :- string_to_atom([C], D), D = '?'.
is_auth(C)      :- string_to_atom([C], D), D = '@'.
is_slash(C)     :- string_to_atom([C], D), D = '/'.
is_dpnt(C)      :- string_to_atom([C], D), D = ':'.
is_point(C)     :- string_to_atom([C], D), D = '.'.

%   ----------------------------------------------------------------------------
%   Funzioni comuni per controllare sequenze di caratteri non validi.
%   ----------------------------------------------------------------------------

is_dslash(C, T) :- string_to_atom([C], D), D = '/',
                   head_cmp(T, "/").
               
is_seqpnt(C, T) :- string_to_atom([C], D), D = '.',
                   head_cmp(T, ".").
                   
is_seqnot(C, T) :- string_to_atom([C], D), D = '.',
                   (
                        head_cmp(T, "#");
                        head_cmp(T, "?");
                        head_cmp(T, "/");
                        head_cmp(T, ":")
                   ).
                   
is_segseq(T) :- (
                    head_cmp(T, "#");
                    head_cmp(T, "?");
                    head_cmp(T, "/");
                    head_cmp(T, ":");
                    head_cmp(T, "@")
                ).

%   ----------------------------------------------------------------------------
%   Funzioni comuni su testa/coda della lista.
%   ----------------------------------------------------------------------------

head_cmp([H | _], [H]) :-!.
tail_rtn([_ | T], T).

%   ----------------------------------------------------------------------------
%   Funzione per determinare se il char è un valore numerico tra 0 e 9.
%   ----------------------------------------------------------------------------

is_c_digit(X):- ((number(X), X >= 48, X =< 57); (fail)), !. 
  
%   ----------------------------------------------------------------------------
%   PROTOCOLLO "MAILTO"
%   Scorro la lista char-by-char per verificare la presenza segmento MAILTO.
%   ----------------------------------------------------------------------------

is_mail_type([H | _]):- is_dpnt(H), !, true.
is_mail_type([H | T]):-
    not(T = []),
    not(is_pragma(H)),
    not(is_quest(H)),
    not(is_auth(H)),
    not(is_dpnt(H)),
    not(is_point(H)),
    (
        (head_cmp([H], "m"),
         head_cmp(T, "a"));
        (head_cmp([H], "a"),
         head_cmp(T, "i"));
        (head_cmp([H], "i"),
         head_cmp(T, "l"));
        (head_cmp([H], "l"),
         head_cmp(T, "t"));
        (head_cmp([H], "t"),
         head_cmp(T, "o"));
        (head_cmp([H], "o"),
         head_cmp(T, ":"))
    ), !,
    is_mail_type(T).
  
%   ----------------------------------------------------------------------------
%   PROTOCOLLO "NEWS"
%   Scorro la lista char-by-char per verificare la presenza segmento NEWS.
%   ----------------------------------------------------------------------------

is_news_type([H | _]):- is_dpnt(H), !, true.
is_news_type([H | T]):-
    not(T = []),
    not(is_pragma(H)),
    not(is_quest(H)),
    not(is_auth(H)),
    not(is_dpnt(H)),
    not(is_point(H)),
    (
        (head_cmp([H], "n"),
         head_cmp(T, "e"));
        (head_cmp([H], "e"),
         head_cmp(T, "w"));
        (head_cmp([H], "w"),
         head_cmp(T, "s"));
        (head_cmp([H], "s"),
         head_cmp(T, ":"))
    ), !,
    is_news_type(T).
  
%   ----------------------------------------------------------------------------
%   PROTOCOLLO "TEL"
%   Scorro la lista char-by-char per verificare la presenza segmento TEL.
%   ----------------------------------------------------------------------------

is_tel_type([H | _]):- is_dpnt(H), !, true.
is_tel_type([H | T]):-
    not(T = []),
    not(is_pragma(H)),
    not(is_quest(H)),
    not(is_auth(H)),
    not(is_dpnt(H)),
    not(is_point(H)),
    (
        (head_cmp([H], "t"),
         head_cmp(T, "e"));
        (head_cmp([H], "e"),
         head_cmp(T, "l"));
        (head_cmp([H], "l"),
         head_cmp(T, ":"))
    ), !,
    is_tel_type(T).
  
%   ----------------------------------------------------------------------------
%   PROTOCOLLO "FAX"
%   Scorro la lista char-by-char per verificare la presenza segmento FAX.
%   ----------------------------------------------------------------------------

is_fax_type([H | _]):- is_dpnt(H), !, true.
is_fax_type([H | T]):-
    not(T = []),
    not(is_pragma(H)),
    not(is_quest(H)),
    not(is_auth(H)),
    not(is_dpnt(H)),
    not(is_point(H)),
    (
        (head_cmp([H], "f"),
         head_cmp(T, "a"));
        (head_cmp([H], "a"),
         head_cmp(T, "x"));
        (head_cmp([H], "x"),
         head_cmp(T, ":"))
    ), !,
    is_fax_type(T).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                            FUNZIONE PRINCIPALE                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   ----------------------------------------------------------------------------
%   Scompongo la stringa URI passata in input in 7 segmenti.
%   ----------------------------------------------------------------------------

parsed_uri(URIString, 
           uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    not(URIString = []), !,
    uri_type(URIString, uri(Sc, Au, Ho, Po, Pa, Qe, Pr)),
    string_to_atom(Sc, Scheme),
    string_to_atom(Au, Userinfo),
    string_to_atom(Ho, Host),
    string_to_atom(Po, Port),
    string_to_atom(Pa, Path),
    string_to_atom(Qe, Query),
    string_to_atom(Pr, Fragment).
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                            FUNZIONI - URI TYPE                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   ----------------------------------------------------------------------------
%   Se il protocollo è "TEL" o "FAX" passo il controllo a TF_SCHEME( L ).
%   ----------------------------------------------------------------------------

uri_type(L, uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    (
        is_tel_type(L);
        is_fax_type(L)
    ), !,
    tf_scheme(L, uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)).
    
%   ----------------------------------------------------------------------------
%   Se il protocollo è "MAILTO" passo il controllo a MAIL_SCHEME( L ).
%   ----------------------------------------------------------------------------

uri_type(L, uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    is_mail_type(L), !,
    mail_scheme(L, uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)).

%   ----------------------------------------------------------------------------
%   Se il protocollo è "NEWS" passo il controllo a NEWS_SCHEME( L ).
%   ----------------------------------------------------------------------------

uri_type(L, uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    is_news_type(L), !,
    news_scheme(L, uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)).
    
%   ----------------------------------------------------------------------------
%   Se il protocollo non è "TEL", "FAX", Ecc passo il controllo a SCHEME( L ).
%   ----------------------------------------------------------------------------

uri_type(L, uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    not(is_tel_type(L)),
    not(is_fax_type(L)), !,
    scheme(L, uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%------------------------------------------------------------------------------%
%                                                                              %
%                                GESTIONE RAMO                                 %
%                              PROTOCOLLO - NEWS                               %
%                                                                              %
%------------------------------------------------------------------------------%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           ESTRAGGO URI NEWS SCHEMA                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   ----------------------------------------------------------------------------
%   Se trovo il carattere ":" privo "/" passo il controllo a NEWS_HOST( L ).
%   ----------------------------------------------------------------------------

news_scheme([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    is_dpnt(H),
    not(T = []),
    not(head_cmp(T, "/")), !,
    news_host(T, uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)).
    
%   ----------------------------------------------------------------------------
%   Scorro la lista char-by-char per estrarre il segmento scheme.
%   ----------------------------------------------------------------------------

news_scheme([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    not(T = []),
    not(is_pragma(H)),
    not(is_quest(H)),
    not(is_auth(H)),
    not(is_dpnt(H)),
    not(is_point(H)),
    not(is_slash(H)), !,
    news_scheme(T, uri(L, Userinfo, Host, Port, Path, Query, Fragment)),
    append([H], L, Scheme).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                            ESTRAGGO URI NEWS HOST                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   ----------------------------------------------------------------------------
%   Se lista vuota blocco la ricorsione di Host.
%   ----------------------------------------------------------------------------

news_host([], uri([], [], [], [], [], [], [])):-!.

%   ----------------------------------------------------------------------------
%   Se la coda è vuota: salvo Host e blocco la ricorsione.
%   ----------------------------------------------------------------------------

news_host([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    T = [],
    not(is_pragma(H)),
    not(is_quest(H)),
    not(is_slash(H)),
    not(is_dpnt(H)),
    not(is_point(H)),
    not(is_auth(H)), !,
    news_host(T, uri(Scheme, Userinfo, L, Port, Path, Query, Fragment)),
    append([H], L, Host).

%   ----------------------------------------------------------------------------
%   Scorro la lista char-by-char per estrarre il segmento Host.
%   ----------------------------------------------------------------------------

news_host([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    not(T = []),
    not(is_pragma(H)),
    not(is_quest(H)),
    not(is_slash(H)),
    not(is_dpnt(H)),
    not(is_auth(H)),
    not(is_seqpnt(H, T)), !,
    news_host(T, uri(Scheme, Userinfo, L, Port, Path, Query, Fragment)),
    append([H], L, Host).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%------------------------------------------------------------------------------%
%                                                                              %
%                                GESTIONE RAMO                                 %
%                             PROTOCOLLO - MAILTO                              %
%                                                                              %
%------------------------------------------------------------------------------%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           ESTRAGGO URI MAIL SCHEMA                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   ----------------------------------------------------------------------------
%   Se trovo il carattere ":" privo "/" passo il controllo a MAIL_AUTH( L ).
%   ----------------------------------------------------------------------------

mail_scheme([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    is_dpnt(H),
    not(head_cmp(T, "/")), !,
    mail_auth(T, uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)).
    
%   ----------------------------------------------------------------------------
%   Scorro la lista char-by-char per estrarre il segmento scheme.
%   ----------------------------------------------------------------------------

mail_scheme([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    not(T = []),
    not(is_pragma(H)),
    not(is_quest(H)),
    not(is_auth(H)),
    not(is_dpnt(H)),
    not(is_point(H)),
    not(is_slash(H)), !,
    mail_scheme(T, uri(L, Userinfo, Host, Port, Path, Query, Fragment)),
    append([H], L, Scheme).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                          ESTRAGGO URI MAIL AUTHORITY                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   ----------------------------------------------------------------------------
%   Se trovo il carattere "@" e T!=[] passo il controllo a MAIL_HOST( L ).
%   ----------------------------------------------------------------------------

mail_auth([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    not(T = []),
    is_auth(H),
    not(head_cmp(T, "#")),
    not(head_cmp(T, "?")),
    not(head_cmp(T, "@")),
    not(head_cmp(T, "/")),
    not(head_cmp(T, ".")),
    not(head_cmp(T, ":")), !,
    mail_host(T, uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)).

%   ----------------------------------------------------------------------------
%   Se la coda è vuota: salvo authority e blocco la ricorsione.
%   ----------------------------------------------------------------------------

mail_auth([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    T = [],
    not(is_pragma(H)),
    not(is_quest(H)),
    not(is_slash(H)),
    not(is_dpnt(H)),
    not(is_auth(H)), !,
    mail_host(T, uri(Scheme, L, Host, Port, Path, Query, Fragment)),
    append([H], L, Userinfo).

%   ----------------------------------------------------------------------------
%   Scorro la lista char-by-char per estrarre il segmento authority.
%   ----------------------------------------------------------------------------

mail_auth([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    not(T = []),
    not(is_pragma(H)),
    not(is_quest(H)),
    not(is_slash(H)),
    not(is_dpnt(H)),
    not(is_auth(H)), !,
    mail_auth(T, uri(Scheme, L, Host, Port, Path, Query, Fragment)),
    append([H], L, Userinfo).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                            ESTRAGGO URI MAIL HOST                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   ----------------------------------------------------------------------------
%   Se lista vuota blocco la ricorsione di Host.
%   ----------------------------------------------------------------------------

mail_host([], uri([], [], [], [], [], [], [])):-!.

%   ----------------------------------------------------------------------------
%   Se la coda è vuota: salvo Host e blocco la ricorsione.
%   ----------------------------------------------------------------------------

mail_host([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    T = [],
    not(is_pragma(H)),
    not(is_quest(H)),
    not(is_slash(H)),
    not(is_dpnt(H)),
    not(is_point(H)),
    not(is_auth(H)), !,
    mail_host(T, uri(Scheme, Userinfo, L, Port, Path, Query, Fragment)),
    append([H], L, Host).

%   ----------------------------------------------------------------------------
%   Scorro la lista char-by-char per estrarre il segmento Host.
%   ----------------------------------------------------------------------------

mail_host([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    not(T = []),
    not(is_pragma(H)),
    not(is_quest(H)),
    not(is_slash(H)),
    not(is_dpnt(H)),
    not(is_auth(H)),
    not(is_seqpnt(H, T)), !,
    mail_host(T, uri(Scheme, Userinfo, L, Port, Path, Query, Fragment)),
    append([H], L, Host).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%------------------------------------------------------------------------------%
%                                                                              %
%                                GESTIONE RAMO                                 %
%                            PROTOCOLLO - TEL  FAX                             %
%                                                                              %
%------------------------------------------------------------------------------%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                         ESTRAGGO URI TEL/FAX SCHEMA                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   ----------------------------------------------------------------------------
%   Se trovo il carattere ":" privo "/" passo il controllo a TF_AUTH( L ).
%   ----------------------------------------------------------------------------

tf_scheme([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    not(T = []),
    is_dpnt(H),
    not(head_cmp(T, "/")), !,
    tf_auth(T, uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)).
    
%   ----------------------------------------------------------------------------
%   Scorro la lista char-by-char per estrarre il segmento scheme.
%   ----------------------------------------------------------------------------

tf_scheme([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    not(T = []),
    not(is_pragma(H)),
    not(is_quest(H)),
    not(is_auth(H)),
    not(is_dpnt(H)),
    not(is_point(H)),
    not(is_slash(H)), !,
    tf_scheme(T, uri(L, Userinfo, Host, Port, Path, Query, Fragment)),
    append([H], L, Scheme).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                        ESTRAGGO URI TEL/FAX AUTHORITY                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   ----------------------------------------------------------------------------
%   Se lista vuota blocco la ricorsione di Authority.
%   ----------------------------------------------------------------------------

tf_auth([], uri([], [], [], [], [], [], [])):-!.

%   ----------------------------------------------------------------------------
%   Se la coda è vuota: salvo authority e blocco la ricorsione.
%   ----------------------------------------------------------------------------

tf_auth([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    T = [],
    not(is_pragma(H)),
    not(is_quest(H)),
    not(is_slash(H)),
    not(is_dpnt(H)),
    not(is_auth(H)), !,
    tf_auth(T, uri(Scheme, L, Host, Port, Path, Query, Fragment)),
    append([H], L, Userinfo).

%   ----------------------------------------------------------------------------
%   Scorro la lista char-by-char per estrarre il segmento authority.
%   ----------------------------------------------------------------------------

tf_auth([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    not(T = []),
    not(is_pragma(H)),
    not(is_quest(H)),
    not(is_slash(H)),
    not(is_dpnt(H)),
    not(is_auth(H)), !,
    tf_auth(T, uri(Scheme, L, Host, Port, Path, Query, Fragment)),
    append([H], L, Userinfo).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%------------------------------------------------------------------------------%
%                                                                              %
%                                GESTIONE RAMO                                 %
%                            PROTOCOLLO - GENERICO                             %
%                                                                              %
%------------------------------------------------------------------------------%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                            ESTRAGGO URI SCHEMA                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   ----------------------------------------------------------------------------
%   Se trovo il carattere ":" ed è "://" passo il controllo a C_AUTH( L ).
%   ----------------------------------------------------------------------------

scheme([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    is_dpnt(H),
    head_cmp(T, "/"),
    tail_rtn(T, TN),
    head_cmp(TN, "/"),
    tail_rtn(TN, TM),
    not(head_cmp(TM, "#")),
    not(head_cmp(TM, "?")),
    not(head_cmp(TM, "@")),
    not(head_cmp(TM, "/")),
    not(head_cmp(TM, ".")),
    not(head_cmp(TM, ":")), !,
    c_auth(TM, TM, uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)).
    
%   ----------------------------------------------------------------------------
%   Se trovo il carattere ":" seguito da "/" passo il controllo a PATH( L ).
%   ----------------------------------------------------------------------------

scheme([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    is_dpnt(H),
    head_cmp(T, "/"),
    tail_rtn(T, TM),
    not(head_cmp(TM, "@")),
    not(head_cmp(TM, ".")),
    not(head_cmp(TM, ":")), !,
    path(TM, uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)).
    
%   ----------------------------------------------------------------------------
%   Se trovo il carattere ":" e coda vuota passo il controllo a PATH( L ).
%   ----------------------------------------------------------------------------

scheme([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    is_dpnt(H),
    T = [], !,
    path(T, uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)).
    
%   ----------------------------------------------------------------------------
%   Se trovo il carattere ":" privo "/" passo il controllo a PATH( L ).
%   ----------------------------------------------------------------------------

scheme([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    is_dpnt(H),
    not(head_cmp(T, "@")),
    not(head_cmp(T, ".")),
    not(head_cmp(T, ":")),
    tail_rtn(T, TM),
    not(head_cmp(TM, "#")),
    not(TM = []), !,
    path(T, uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)).
    
%   ----------------------------------------------------------------------------
%   Scorro la lista char-by-char per estrarre il segmento scheme.
%   ----------------------------------------------------------------------------

scheme([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    not(T = []),
    not(is_pragma(H)),
    not(is_quest(H)),
    not(is_auth(H)),
    not(is_dpnt(H)),
    not(is_slash(H)), !,
    scheme(T, uri(L, Userinfo, Host, Port, Path, Query, Fragment)),
    append([H], L, Scheme).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                          CONTROLLO: PRE-AUTHORITY                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   ----------------------------------------------------------------------------
%   Se trovo una "@" passo il backup della lista ad AUTHORITY( L ).
%   ----------------------------------------------------------------------------

c_auth([H | _], Bak, uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    is_auth(H), !,
    authority(Bak, uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)).
    
%   ----------------------------------------------------------------------------
%   Se trovo una lista vuota passo il backup della lista a HOST( L ).
%   ----------------------------------------------------------------------------

c_auth([H | T], Bak, uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    (
        T = [];
        is_dpnt(H);
        is_pragma(H);
        is_quest(H);
        is_dpnt(H);
        is_slash(H)
    ), !,
    host(Bak, uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)).
    
%   ----------------------------------------------------------------------------
%   Ricorsione su lista clone in cerca di un carattere AUTHORITY.
%   ----------------------------------------------------------------------------

c_auth([H | T], Bak, uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    not(T = []),
    not(is_pragma(H)),
    not(is_quest(H)),
    not(is_auth(H)),
    not(is_dpnt(H)),
    not(is_slash(H)), !,
    c_auth(T, Bak, uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           ESTRAGGO URI AUTHORITY                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   ----------------------------------------------------------------------------
%   Se lista vuota blocco la ricorsione di Authority.
%   ----------------------------------------------------------------------------

authority([], uri([], [], [], [], [], [], [])):-!.

%   ----------------------------------------------------------------------------
%   Se trovo il carattere "@" passo il controllo a HOST( L ).
%   ----------------------------------------------------------------------------

authority([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    is_auth(H),
    not(T = []),
    not(head_cmp(T, "#")),
    not(head_cmp(T, "?")),
    not(head_cmp(T, "@")),
    not(head_cmp(T, "/")),
    not(head_cmp(T, ".")),
    not(head_cmp(T, ":")),  !,
    host(T, uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)).

%   ----------------------------------------------------------------------------
%   Scorro la lista char-by-char per estrarre il segmento authority.
%   ----------------------------------------------------------------------------

authority([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    not(T = []),
    not(is_pragma(H)),
    not(is_quest(H)),
    not(is_dpnt(H)),
    not(is_auth(H)), !,
    authority(T, uri(Scheme, L, Host, Port, Path, Query, Fragment)),
    append([H], L, Userinfo).
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                             ESTRAGGO URI HOST                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   ----------------------------------------------------------------------------
%   Se lista vuota blocco la ricorsione su Host.
%   ----------------------------------------------------------------------------

host([], uri([], [], [], [], [], [], [])):-!.

%   ----------------------------------------------------------------------------
%   Se la coda è vuota: salvo host e blocco la ricorsione.
%   ----------------------------------------------------------------------------

host([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    T = [],
    not(is_pragma(H)),
    not(is_quest(H)),
    not(is_slash(H)),
    not(is_point(H)),
    not(is_dpnt(H)), !,
    host(T, uri(Scheme, Userinfo, L, Port, Path, Query, Fragment)),
    append([H], L, Host).
    
%   ----------------------------------------------------------------------------
%   Se trovo il carattere ":" privo "/" passo il controllo a PORT( L ).
%   ----------------------------------------------------------------------------

host([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    is_dpnt(H),
    not(T = []),
    not(head_cmp(T, "/")), !,
    port(T, uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)).
    
%   ----------------------------------------------------------------------------
%   Se trovo il carattere "/" privo "//" passo il controllo a PATH( L ).
%   ----------------------------------------------------------------------------
    
host([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    is_slash(H),
    not(is_dslash(H, T)), !,
    path(T, uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)).
    
%   ----------------------------------------------------------------------------
%   Scorro la lista char-by-char per estrarre il segmento host.
%   ----------------------------------------------------------------------------

host([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    not(T = []),
    not(is_auth(H)),
    not(is_pragma(H)),
    not(is_quest(H)),
    not(is_dpnt(H)),
    not(is_slash(H)),
    not(is_seqpnt(H, T)),
    not(is_seqnot(H, T)), !,
    host(T, uri(Scheme, Userinfo, L, Port, Path, Query, Fragment)),
    append([H], L, Host).
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                             ESTRAGGO URI PORT                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   ----------------------------------------------------------------------------
%   Se lista vuota blocco la ricorsione su Port.
%   ----------------------------------------------------------------------------

port([], uri([], [], [], [], [], [], [])):-!.
    
%   ----------------------------------------------------------------------------
%   Se trovo il carattere "/" passo il controllo a PATH( L ).
%   ----------------------------------------------------------------------------

port([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    is_slash(H), !,
    path(T, uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)).
    
%   ----------------------------------------------------------------------------
%   Se la coda è vuota: salvo port e blocco la ricorsione.
%   ----------------------------------------------------------------------------

port([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    T = [],
    is_c_digit(H), !,
    port(T, uri(Scheme, Userinfo, Host, L, Path, Query, Fragment)),
    append([H], L, Port).

%   ----------------------------------------------------------------------------
%   Scorro la lista char-by-char per estrarre il segmento port.
%   ----------------------------------------------------------------------------

port([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    not(T = []),
    is_c_digit(H), !,
    port(T, uri(Scheme, Userinfo, Host, L, Path, Query, Fragment)),
    append([H], L, Port).
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                             ESTRAGGO URI PATH                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   ----------------------------------------------------------------------------
%   Se lista vuota blocco la ricorsione su Path.
%   ----------------------------------------------------------------------------

path([], uri([], [], [], [], [], [], [])):-!.

%   ----------------------------------------------------------------------------
%   Se trovo il carattere "/" con "?" o "#" termino la ricorsione con FAIL.
%   ----------------------------------------------------------------------------

path([H | T], uri(_, _, _, _, _, _, _)):-
    is_slash(H), 
    (
        is_segseq(T);
        T = []
    ), !,
    fail.

%   ----------------------------------------------------------------------------
%   Se trovo il carattere "?" passo il controllo a QUERY( L ).
%   ----------------------------------------------------------------------------

path([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    is_quest(H),
    not(head_cmp(T, "#")),
    not(T = []), !,
    query(T, uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)).
    
%   ----------------------------------------------------------------------------
%   Se trovo il carattere "#" passo il controllo a PRAGMA( L ).
%   ----------------------------------------------------------------------------
    
path([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    is_pragma(H),
    not(T = []), !,
    pragma(T, uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)).
    
%   ----------------------------------------------------------------------------
%   Se la coda è vuota: salvo path e blocco la ricorsione.
%   ----------------------------------------------------------------------------

path([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    T = [],
    not(is_auth(H)),
    not(is_dpnt(H)),
    not(is_pragma(H)),
    not(is_quest(H)),
    not(is_slash(H)), !,
    path(T, uri(Scheme, Userinfo, Host, Port, L, Query, Fragment)),
    append([H], L, Path).
    
%   ----------------------------------------------------------------------------
%   Scorro la lista char-by-char per estrarre il segmento path.
%   ----------------------------------------------------------------------------

path([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    not(T = []),
    not(is_dpnt(H)),
    not(is_auth(H)),
    not(is_pragma(H)),
    not(is_quest(H)),
    not(is_dslash(H, T)), !,
    path(T, uri(Scheme, Userinfo, Host, Port, L, Query, Fragment)),
    append([H], L, Path).
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                            ESTRAGGO URI QUERY                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   ----------------------------------------------------------------------------
%   Se lista vuota blocco la ricorsione su Query.
%   ----------------------------------------------------------------------------

query([], uri([], [], [], [], [], [], [])):-!.

%   ----------------------------------------------------------------------------
%   Se trovo il carattere "#" passo il controllo a PRAGMA( L ).
%   ----------------------------------------------------------------------------

query([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    is_pragma(H),
    not(T = []), !,
    pragma(T, uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)).

%   ----------------------------------------------------------------------------
%   Se la coda è vuota: salvo query e blocco la ricorsione.
%   ----------------------------------------------------------------------------

query([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    T = [],
    not(is_pragma(H)), !,
    query(T, uri(Scheme, Userinfo, Host, Port, Path, L, Fragment)),
    append([H], L, Query).
    
%   ----------------------------------------------------------------------------
%   Scorro la lista char-by-char per estrarre il segmento query.
%   ----------------------------------------------------------------------------

query([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    not(T = []),
    not(is_pragma(H)), !,
    query(T, uri(Scheme, Userinfo, Host, Port, Path, L, Fragment)),
    append([H], L, Query).
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                            ESTRAGGO URI PRAGMA                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   ----------------------------------------------------------------------------
%   Se lista vuota blocco la ricorsione su Pragma/Fragment.
%   ----------------------------------------------------------------------------

pragma([], uri([], [], [], [], [], [], [])):-!.

%   ----------------------------------------------------------------------------
%   Scorro la lista char-by-char per estrarre il segmento pragma/fragment.
%   ----------------------------------------------------------------------------

pragma([H | T], uri(Scheme, Userinfo, Host, Port, Path, Query, Fragment)):-
    pragma(T, uri(Scheme, Userinfo, Host, Port, Path, Query, L)),
    append([H], L, Fragment).
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                            FINE INTERPRETE URI                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

